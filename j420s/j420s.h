#undef UNICODE

#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <iostream>
#include <sstream>

using std::stringstream;
using std::ostringstream;
using std::string;
using std::cout;
using std::endl;

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "10187"
#define J420S_VERSION "1.2.0"

int SocketInit(int, WSADATA*);
int SocketAddrInfo(int, addrinfo*, addrinfo**);
SOCKET SocketCreate(SOCKET, addrinfo* );
int SocketBind(int, SOCKET, addrinfo* );
int SocketListen(int, SOCKET, addrinfo* );
SOCKET SocketAcceptConnection(SOCKET, SOCKET );
const char* ReturnSocketStatus(string, int);
int CreateMyProcess(LPCTSTR);
string GetDwordRegistryValue(LPCTSTR);
string GetLocalIp();
string GetMyUserName();
int DisplayBanner();