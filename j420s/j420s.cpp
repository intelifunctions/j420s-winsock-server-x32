/*
Author: Jason Edelbrock
Date: 02/19/2016 10:46:32:PM
Last Modified: 02/19/2016 10:46:32:PM
Original Source: MSDN Winsock Server Code
Source Link: https://msdn.microsoft.com/en-us/library/windows/desktop/ms737593(v=vs.85).aspx
*/

#include "j420s.h"

int __cdecl main(void) {

	if (DisplayBanner() != 0){

		cout << "Unable to display banner!" << endl;

	}

	WSADATA wsaData;
	int iResult = NULL;

	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *MySocketResult = NULL;
	struct addrinfo MySocket;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	ZeroMemory(&MySocket, sizeof(MySocket));
	MySocket.ai_family = AF_INET;
	MySocket.ai_socktype = SOCK_STREAM;
	MySocket.ai_protocol = IPPROTO_TCP;
	MySocket.ai_flags = AI_PASSIVE;

	iResult = SocketInit(iResult, &wsaData);
	
	if (iResult == 1){

		return 1;

	}

	iResult = SocketAddrInfo(iResult, &MySocket, &MySocketResult);

	if (iResult == 1){

		return 1;

	}

	ListenSocket = SocketCreate(ListenSocket, MySocketResult);
	
	if (ListenSocket == 1){

		return 1;

	}
	
	iResult = SocketBind(iResult, ListenSocket, MySocketResult);
	
	if (iResult == 1) {
		
		return 1;

	}

	iResult = SocketListen(iResult, ListenSocket, MySocketResult);
	
	if (iResult == 1) {
		
		return 1;

	}

	cout << "Running under user: " << GetMyUserName() << endl;
	cout << "Unique ID: " << GetDwordRegistryValue("ProductID") << endl;
	cout << "Listening for connection on: " << GetLocalIp() << ":" << DEFAULT_PORT << endl;

	ClientSocket = accept(ListenSocket, NULL, NULL);
	
	if (ClientSocket == INVALID_SOCKET) {
		
		printf("Accept failed with error: %d\n", WSAGetLastError());
		
		closesocket(ListenSocket);
		WSACleanup();

		return 1;

	}

	closesocket(ListenSocket);

	while (1 == 1){
		
		do {

			iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
			
			if (iResult > 0) {
				
				printf("Bytes received: %d\n", iResult);

				// Echo the buffer back to the sender
				iSendResult = send(ClientSocket, recvbuf, iResult, 0);
				
				if (iSendResult == SOCKET_ERROR) {
					
					printf("send failed with error: %d\n", WSAGetLastError());
					
					closesocket(ClientSocket);
					WSACleanup();

					return 1;

				}

				printf("Bytes sent: %d\n", iSendResult);

			}

		} while (iResult > 0);

	}
	
	iResult = shutdown(ClientSocket, SD_SEND);
	
	if (iResult == SOCKET_ERROR) {
		
		printf("Shutdown failed with error: %d\n", WSAGetLastError());
		
		closesocket(ClientSocket);
		WSACleanup();

		return 1;

	}

	closesocket(ClientSocket);
	WSACleanup();
	
	return 0;

}

// Get address info function.
int SocketAddrInfo(int iResult, addrinfo* MySocket, addrinfo** MySocketResult){
	
	iResult = getaddrinfo(NULL, DEFAULT_PORT, MySocket, MySocketResult);
	
	if (iResult != 0) {
		
		printf("Get address info failed with error: %d\n", iResult);
		
		WSACleanup();
		
		std::cout << "Server closing in 5 ";

		for (int i = 4; i > 0; i--){

			Sleep(1 * 1000);

			cout << i << " ";

		}

		cout << "Server closing now!" << endl;

		return 1;

	}

	return iResult;

}

// Socket initialization.
int SocketInit(int iResult, WSADATA *wsaData){

	iResult = WSAStartup(MAKEWORD(2, 2), wsaData);
	
	if (iResult != 0) {
		
		printf("WSAStartup failed with error: %d\n", iResult);
		
		std::cout << "Server closing in 5 ";

		for (int i = 4; i > 0; i--){

			Sleep(1 * 1000);

			cout << i << " ";

		}

		cout << "Server closing now!" << endl;

		return 1;

	}

	return iResult;

}

// Socket create function to create a socket for connecting to our server.
SOCKET SocketCreate(SOCKET ListenSocket, addrinfo* MySocketResult){
	
	ListenSocket = socket( MySocketResult->ai_family, MySocketResult->ai_socktype, MySocketResult->ai_protocol );
	
	if ( ListenSocket == INVALID_SOCKET ) {
		
		printf("Socket failed with error: %ld\n", WSAGetLastError());
		
		freeaddrinfo(MySocketResult);
		WSACleanup();
		
		std::cout << "Server closing in 5 ";
		
		for (int i = 4; i > 0; i--){
			
			Sleep(1 * 1000);
			
			cout << i << " ";

		}
		
		cout << "Server closing now!" << endl;

		return 1;

	}
	
	return ListenSocket;

}

// Socket bind function for binding our socket to an address for incoming connections.
int SocketBind(int iResult, SOCKET ListenSocket, addrinfo* MySocketResult) {

	iResult = bind(ListenSocket, MySocketResult->ai_addr, (int)MySocketResult->ai_addrlen);
	
	if (iResult == SOCKET_ERROR) {
		
		printf("Bind failed with error: %d\n", WSAGetLastError());
		
		freeaddrinfo(MySocketResult);
		closesocket(ListenSocket);
		WSACleanup();
		
		std::cout << "Server closing in 5 ";

		for (int i = 4; i > 0; i--){

			Sleep(1 * 1000);

			cout << i << " ";

		}

		cout << "Server closing now!" << endl;

		return 1;

	}

	return iResult;

}

// Socket listen function to listen for incoming connections.
int SocketListen(int iResult, SOCKET ListenSocket, addrinfo* MySocketResult) {

	freeaddrinfo(MySocketResult);

	iResult = listen(ListenSocket, SOMAXCONN);
	
	if (iResult == SOCKET_ERROR) {
		
		printf("Listen failed with error: %d\n", WSAGetLastError());
		
		closesocket(ListenSocket);
		WSACleanup();
		
		std::cout << "Server closing in 5 ";

		for (int i = 4; i > 0; i--){

			Sleep(1 * 1000);

			cout << i << " ";

		}

		cout << "Server closing now!" << endl;

		return 1;

	}

	return iResult;

}

// Socket accept connection function to accept an incoming connection.
SOCKET SocketAcceptConnection(SOCKET ClientSocket, SOCKET ListenSocket) {

	ClientSocket = accept(ListenSocket, NULL, NULL);
	
	if (ClientSocket == INVALID_SOCKET) {
		
		int EventResult = 1;

		printf("Returned Error: %d\n%s ", WSAGetLastError(), ReturnSocketStatus("WSOCK_ACCEPTCON", EventResult));
		
		closesocket(ListenSocket);
		WSACleanup();
		
		std::cout << "Server closing in 5 ";

		for (int i = 4; i > 0; i--){

			Sleep(1 * 1000);

			cout << i << " ";

		}

		cout << "Server closing now!" << endl;

		return 1;

	}

	closesocket(ListenSocket);

	return 0;

}

// Error handler function.
const char* ReturnSocketStatus(string ErrorSection, int Id) {

	if (ErrorSection == "WSOCK_INI") {

		const char* WSOCK_INI[2] = {

			"SUCCESS!",
			"SOCKET INITIALIZATION ERROR!"

		};

		return WSOCK_INI[Id];

	}

	if (ErrorSection == "WSOCK_ACCEPTCON") {

		const char* WSOCK_ACCEPTCON[2] = {

			"SUCCESS!",
			"SOCKET ACCEPT CONNECTION ERROR!"

		};

		return WSOCK_ACCEPTCON[Id];

	}

	return "UNKNOWN ERROR";

}

// Create process function.
int CreateMyProcess(LPCTSTR AppName) {

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	LPCTSTR lpApplicationName = AppName;

	if (!CreateProcess(lpApplicationName, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &si, &pi)) {

		std::cerr << "CreateProcess() failed to start program \"" << lpApplicationName << "\"\n";

		return -1;

	}

	cout << "Started program \"" << lpApplicationName << "\" successfully\n";

	return 0;

}

// Get registry value function.
string GetDwordRegistryValue(LPCTSTR SubkeyName) {

	HKEY hKey = NULL;
	LPCTSTR SubkeyLoc = ("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\DefaultProductKey");

	if (RegOpenKey(HKEY_LOCAL_MACHINE, SubkeyLoc, &hKey) != ERROR_SUCCESS) {

		cout << "Unable to open registry at key location: " << SubkeyLoc << endl;

	}

	TCHAR SubkeyBuffValue[1024];
	DWORD SubkeyLengthValue = sizeof(SubkeyBuffValue);

	if (RegQueryValueEx(hKey, SubkeyName, NULL, NULL, reinterpret_cast <LPBYTE> (&SubkeyBuffValue), &SubkeyLengthValue) != ERROR_SUCCESS) {

		cout << "Unable to query subkey: " << SubkeyName << endl;

	}

	return string(SubkeyBuffValue);

}

// Get local ip function.
string GetLocalIp() {

	char HostnameBuffer[128];

	if (gethostname(HostnameBuffer, sizeof(HostnameBuffer)) == SOCKET_ERROR) {

		printf("Error: %d\n", WSAGetLastError());

	}

	struct hostent *Host = gethostbyname(HostnameBuffer);

	if (Host == 0) {

		printf("Hostname lookup failed! Error: %d\n", WSAGetLastError());

	}

	struct in_addr HostAddr;
	memcpy(&HostAddr, Host->h_addr_list[0], sizeof(struct in_addr));

	return inet_ntoa(HostAddr);

}

// Get system username function.
string GetMyUserName() {

	char acUserName[128];
	DWORD nUserName = sizeof(acUserName);

	GetUserName(acUserName, &nUserName);

	stringstream StrStream;
	string UserName;

	StrStream << acUserName;
	StrStream >> UserName;

	return UserName;

}

// Display banner function.
int DisplayBanner() {

	cout << " _____________________________________________________________________________\n" <<
		"|    <-------- J420S " << J420S_VERSION << " ---------------------------------------------->    |\n" <<
		"|    <-------- Author: Jason Edelbrock ---------------------------------->    |\n" <<
		"|    <-------- Date: 02/19/2016 ----------------------------------------->    |\n" <<
		"|_____________________________________________________________________________|\n\n";

	return 0;

}